package app

import (
	"flag"
	"fmt"
	"gotoko/database/seeders"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/urfave/cli"
	"gorm.io/gorm"
)

// Connect To Server
type Server struct {
	DB     *gorm.DB
	Router *mux.Router
}

// Environment Configuration
type EnvironmentConfig struct {
	AppName  string
	AppEnv   string
	AppPort  string
	DBHost   string
	DBPort   string
	DBUser   string
	DBPass   string
	DBName   string
	DBDriver string
}

func (server *Server) Initialize(envConfig EnvironmentConfig) {
	fmt.Println("Welcome to " + envConfig.AppName)
	server.initializeRoute()
}

func (server *Server) Run(addr string) {
	fmt.Printf("Listening on port %s", addr)
	log.Fatal(http.ListenAndServe(addr, server.Router))
}

func getEnv(key string, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}

func Run() {
	var server = Server{}
	var envConfig = EnvironmentConfig{}

	err := godotenv.Load()

	if err != nil {
		log.Fatal("Error on loading .env file")
	}

	// App Config
	envConfig.AppName = getEnv("APP_NAME", "Go Toko")
	envConfig.AppEnv = getEnv("APP_ENV", "development")
	envConfig.AppPort = getEnv("APP_PORT", "9000")

	// Database Config
	envConfig.DBHost = getEnv("DB_HOSTNAME", "localhost")
	envConfig.DBPort = getEnv("DB_PORT", "5432")
	envConfig.DBDriver = getEnv("DB_DRIVER", "mysql")
	envConfig.DBUser = getEnv("DB_USERNAME", "postgres")
	envConfig.DBPass = getEnv("DB_PASSWORD", "password")
	envConfig.DBName = getEnv("DB_DATABASE", "go_toko_db")

	flag.Parse()
	args := flag.Arg(0)

	if args != "" {
		server.initCommands(envConfig)
	} else {
		server.Initialize(envConfig)
		server.Run(":" + envConfig.AppPort)
	}

}

func (server *Server) initCommands(envConfig EnvironmentConfig) {

	server.initializeDatabase(envConfig)

	cmdApp := cli.NewApp()
	cmdApp.Commands = []cli.Command{
		{
			Name: "db-migrate",
			Action: func(c *cli.Context) error {
				server.dbMigrate()
				return nil
			},
		},
		{
			Name: "db-seed",
			Action: func(c *cli.Context) error {
				err := seeders.DBSeed(server.DB)
				if err != nil {
					log.Fatal(err)
				}

				return nil
			},
		},
	}

	err := cmdApp.Run(os.Args)

	if err != nil {
		log.Fatal(err)
	}
}
