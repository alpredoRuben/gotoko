package models

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	ID            string  `gorm:"size:36;not null;uniqueIndex;primary_key"`
	Profile       Profile `gorm:"foreignKey:UserID"`
	Username      string  `gorm:"size:255;not null"`
	Email         string  `gorm:"unique:true;not null"`
	Password      string  `gorm:"size:255;not null"`
	RememberToken string  `gorm:"size:255;null"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     gorm.DeletedAt
}
