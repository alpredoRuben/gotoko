package models

import (
	"time"
)

type Profile struct {
	ID          string `gorm:"primary_key;size:36;not null;unique:true;"`
	UserID      string `gorm:"size:36;index"`
	FirstName   string `gorm:"size:255;not null;"`
	LastName    string `gorm:"size:255;not null;"`
	IsPrimary   bool
	Address1    string `gorm:"size:255;"`
	Address2    string `gorm:"size:255;"`
	CityID      string `gorm:"size:100;"`
	ProvinceID  string `gorm:"size:100;"`
	PostalCode  string `gorm:"size:255;"`
	PhoneNumber string `gorm:"size:255;"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
