package app

import (
	"fmt"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// Initialize Database
func (server *Server) initializeDatabase(envConfig EnvironmentConfig) {
	var err error

	var db_host string = envConfig.DBHost
	var db_user string = envConfig.DBUser
	var db_pass string = envConfig.DBPass
	var db_name string = envConfig.DBName
	var db_port string = envConfig.DBPort

	if envConfig.DBDriver == "mysql" {
		dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", db_user, db_pass, db_host, db_port, db_name)
		server.DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	} else {
		dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta", db_host, db_user, db_pass, db_name, db_port)
		server.DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	}

	if err != nil {
		panic("Failed connectiong to the database server")
	}

}

func (server *Server) dbMigrate() {
	// Auto Migrate Model
	for _, model := range RegisterModel() {
		err := server.DB.Debug().AutoMigrate(model.Model)

		if err != nil {
			log.Fatal(err)
		}
	}

	fmt.Println("Database success migrated")
}
