module gotoko

go 1.16

require (
	github.com/bxcodec/faker/v3 v3.6.0
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/gosimple/slug v1.11.2
	github.com/joho/godotenv v1.4.0
	github.com/shopspring/decimal v1.3.1
	github.com/unrolled/render v1.4.0
	github.com/urfave/cli v1.22.5
	gorm.io/driver/mysql v1.1.3
	gorm.io/driver/postgres v1.2.1
	gorm.io/gorm v1.22.2
)
